import itertools
import os
from argparse import ArgumentParser

import torch

import ase.db
from neuralneb import utils
from neuralneb.painn import PaiNN

DEVICE = "cuda" if torch.cuda.is_available() else "cpu"


class LossFn(torch.nn.Module):
    def __init__(self, energy_key, forces_key, rho):
        super().__init__()
        self.energy_key = energy_key
        self.forces_key = forces_key
        self.rho = rho

    def forward(self, batch, result):
        diff_energy = batch[self.energy_key] - result[self.energy_key]
        err_sq_energy = torch.sum(diff_energy ** 2)

        diff_forces = batch[self.forces_key] - result[self.forces_key]
        err_sq_forces = torch.sum(diff_forces ** 2) / 3

        err_sq = self.rho * err_sq_energy + (1 - self.rho) * err_sq_forces
        return err_sq


def main(args):  # pylint: disable=redefined-outer-name
    painn = PaiNN(
        num_interactions=3,
        hidden_state_size=256,
        cutoff=5,
    )
    painn.to(DEVICE)

    loss_fn = LossFn(
        energy_key="energy",
        forces_key="forces",
        rho=0.5,
    )

    optimizer = torch.optim.Adam(painn.parameters(), amsgrad=True, weight_decay=0.01)
    assert os.path.exists(
        args.db
    ), 'Dataset doesn\'t exist. If you are using default settings run "python scripts/download_qm9x.db"'

    dataset = utils.get_dataset(args.db)
    pin_memory = DEVICE == 'cuda'
    collate_atoms = utils.CollateAtoms(pin_memory=pin_memory)
    dataloader = torch.utils.data.DataLoader(
        dataset, batch_size=50, collate_fn=collate_atoms
    )

    step = 0
    epoch = 0
    for epoch in itertools.count(epoch):
        for batch in dataloader:
            batch = utils.batch_to_device(batch, DEVICE)
            step += 1
            optimizer.zero_grad()
            result = painn(batch)
            loss = loss_fn(
                batch=batch,
                result=result,
            )
            loss.backward()
            optimizer.step()

            print(f'Epoch: {epoch}, step: {step}, loss: {loss.item():.2f}')

            if step >= 1000000:
                break

    torch.save(painn, "data/painn_qm9x.sd")


if __name__ == "__main__":
    parser = ArgumentParser()
    parser.add_argument("db", nargs="?", default="data/qm9x.db")
    args = parser.parse_args()
    main(args)
